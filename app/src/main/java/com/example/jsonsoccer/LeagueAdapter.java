package com.example.jsonsoccer;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class LeagueAdapter extends RecyclerView.Adapter<LeagueAdapter.MyViewHolder>{

    private Context mContext;
    private List<League> leagues;

    public LeagueAdapter(Context mContext, List<League> leagues) {
        this.mContext = mContext;
        this.leagues = leagues;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View view = layoutInflater.inflate(R.layout.my_row, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Picasso.get().load(leagues.get(position).getStrBadge())
                .fit()
                .centerCrop()
                .into(holder.strBadge);
        holder.strLeague.setText(leagues.get(position).getStrLeague());
        holder.strDescriptionEN.setText(leagues.get(position).getStrDescriptionEN());

        holder.myWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("https://" + leagues.get(holder.getAdapterPosition()).getStrWebsite()));
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                mContext.startActivity(i);
            }
        });

        int totalFanarts = 0;
        for (String fanart : leagues.get(position).getStrFanarts()) {
            if (fanart.equals("null")) {        //buit
                totalFanarts++;
            }
        }

        if (totalFanarts < 4) {
            holder.myImages.setVisibility(View.VISIBLE);
            holder.myImages.setClickable(true);
            holder.myImages.setOnClickListener(view -> {
                Intent intent = new Intent(mContext, ViewPagerActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putStringArrayListExtra("images", leagues.get(holder.getAdapterPosition()).getStrFanarts());
                mContext.startActivity(intent);
            });
        } else {
            holder.myImages.setVisibility(View.INVISIBLE);
            holder.myImages.setClickable(false);
        }

    }

    @Override
    public int getItemCount() {
        return leagues.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView strBadge;
        TextView strLeague;
        TextView strDescriptionEN;
        Button myWeb;
        Button myImages;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            strBadge = itemView.findViewById(R.id.flag);
            strLeague = itemView.findViewById(R.id.textTitle);
            strDescriptionEN = itemView.findViewById(R.id.textDescription);
            myWeb = itemView.findViewById(R.id.myWeb);
            myImages = itemView.findViewById(R.id.myImages);
        }
    }

}

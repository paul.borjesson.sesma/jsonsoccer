package com.example.jsonsoccer;

import java.util.ArrayList;

public class League {
    String strBadge;
    String strLeague;
    String strDescriptionEN;
    String strWebsite;
    ArrayList<String> strFanarts;

    public League() {
    }

    public League(String strBadge, String strLeague, String strDescriptionEN, String strWebsite, ArrayList<String> strFanarts) {
        this.strBadge = strBadge;
        this.strLeague = strLeague;
        this.strDescriptionEN = strDescriptionEN;
        this.strWebsite = strWebsite;
        this.strFanarts = strFanarts;
    }

    public String getStrBadge() {
        return strBadge;
    }

    public void setStrBadge(String strBadge) {
        this.strBadge = strBadge;
    }

    public String getStrLeague() {
        return strLeague;
    }

    public void setStrLeague(String strLeague) {
        this.strLeague = strLeague;
    }

    public String getStrDescriptionEN() {
        return strDescriptionEN;
    }

    public void setStrDescriptionEN(String strDescriptionEN) {
        this.strDescriptionEN = strDescriptionEN;
    }

    public String getStrWebsite() {
        return strWebsite;
    }

    public void setStrWebsite(String strWebsite) {
        this.strWebsite = strWebsite;
    }

    public ArrayList<String> getStrFanarts() {
        return strFanarts;
    }

    public void setStrFanarts(ArrayList<String> strFanarts) {
        this.strFanarts = strFanarts;
    }
}

package com.example.jsonsoccer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DetailActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    List leagues = new ArrayList<>();
    LeagueAdapter leagueAdapter;
    private static String JSON_URL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        getLeagues();
    }

    private void getLeagues() {
        //get intent
        JSON_URL = "https://www.thesportsdb.com/api/v1/json/2/search_all_leagues.php?c=";
        JSON_URL += getIntent().getStringExtra(MainActivity.EXTRA_NAME_COUNTRY);
        JSON_URL += "&s=Soccer";
        System.out.println(JSON_URL);
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                JSON_URL,
                null,
                response -> {
                    try {
                        JSONArray jsonArray = response.getJSONArray("countrys");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject leagueObject = jsonArray.getJSONObject(i);
                            League league = new League();
                            league.setStrBadge(leagueObject.getString("strBadge"));
                            league.setStrLeague(leagueObject.getString("strLeague"));
                            league.setStrDescriptionEN(leagueObject.getString("strDescriptionEN"));
                            league.setStrWebsite(leagueObject.getString("strWebsite"));
                            ArrayList<String> images = new ArrayList<>();
                            images.add(leagueObject.getString("strFanart1"));
                            images.add(leagueObject.getString("strFanart2"));
                            images.add(leagueObject.getString("strFanart3"));
                            images.add(leagueObject.getString("strFanart4"));
                            league.setStrFanarts(images);
                            leagues.add(league);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    recyclerView = findViewById(R.id.recyclerView);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                    leagueAdapter = new LeagueAdapter(getApplicationContext(), leagues);
                    recyclerView.setAdapter(leagueAdapter);
                },
                error -> Log.d("tag", "onErrorResponse: " + error.getMessage())
        );
        queue.add(jsonObjectRequest);
    }

}
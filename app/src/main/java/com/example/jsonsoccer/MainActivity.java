package com.example.jsonsoccer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private Spinner spinner;
    private ArrayList<Country> countries;
    private CountryAdapter cAdapter;
    public static String EXTRA_NAME_COUNTRY = "com.example.startup.EXTRA_NAME_COUNTRY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //hook
        spinner = (Spinner) findViewById(R.id.spinner);

        initList();
        cAdapter = new CountryAdapter(this, countries);
        spinner.setAdapter(cAdapter);
        spinner.setSelected(false);
        spinner.setSelection(0,true);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Country selectedItem = (Country) parent.getItemAtPosition(position);
                Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                intent.putExtra(EXTRA_NAME_COUNTRY,selectedItem.getCountryName());
                startActivity(intent);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void initList() {
        countries = new ArrayList<>();
        countries.add(new Country(R.drawable.argentina, "Argentina"));
        countries.add(new Country(R.drawable.australia, "Australia"));
        countries.add(new Country(R.drawable.canada, "Canada"));
        countries.add(new Country(R.drawable.congo, "Congo"));
        countries.add(new Country(R.drawable.egypt, "Egypt"));
        countries.add(new Country(R.drawable.france, "France"));
        countries.add(new Country(R.drawable.germany, "Germany"));
        countries.add(new Country(R.drawable.greece, "Greece"));
        countries.add(new Country(R.drawable.india, "India"));
        countries.add(new Country(R.drawable.indonesia, "Indonesia"));
        countries.add(new Country(R.drawable.ireland, "Ireland"));
        countries.add(new Country(R.drawable.italy, "Italy"));
        countries.add(new Country(R.drawable.kenya, "Kenya"));
        countries.add(new Country(R.drawable.mexico, "Mexico"));
        countries.add(new Country(R.drawable.newzeland, "Newzeland"));
        countries.add(new Country(R.drawable.night3, "Night"));
        countries.add(new Country(R.drawable.norway, "Norway"));
        countries.add(new Country(R.drawable.poland, "Poland"));
        countries.add(new Country(R.drawable.qatar, "Qatar"));
        countries.add(new Country(R.drawable.southafrica, "Southafrica"));
        countries.add(new Country(R.drawable.southkorea, "Southkorea"));
        countries.add(new Country(R.drawable.spain, "Spain"));
        countries.add(new Country(R.drawable.uk, "Uk"));
        countries.add(new Country(R.drawable.usa, "Usa"));

    }


}